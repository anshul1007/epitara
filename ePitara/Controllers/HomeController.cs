﻿using DetailFileInfo;
using ePitara.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace ePitara.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Wholesale()
        {
            var records = GetImages("Wholesale");
            return View(records);
        }

        public ActionResult Event(string directoryName)
        {
            var records = GetImages(directoryName);
            return View(records);
        }

        public string RefreshImage()
        {
            var directories = Directory.GetDirectories(Server.MapPath("../images/Uploaded"));
            foreach (var d in directories)
            {
                var dirName = new DirectoryInfo(d).Name;
                var path = Server.MapPath("../images/GalleryImages/" + dirName);

                if (Directory.Exists(path))
                {
                    DirectoryInfo directory = new DirectoryInfo(path);
                    directory.Delete(true);
                }

                Directory.CreateDirectory(path);
                Directory.CreateDirectory(path + "/ImagePath");
                Directory.CreateDirectory(path + "/ThumbPath");

                var files = Directory.GetFiles(d);
                foreach (var file in files)
                {
                    var image = Image.FromFile(file);
                    Size imgLarge = new Size(1200, 800);
                    resizeImage(image, imgLarge, "../images/GalleryImages/" + dirName + "/ImagePath/" + Path.GetFileName(file).ToLower());
                    Size imgMedium = new Size(348, 261);
                    resizeImage(image, imgMedium, "../images/GalleryImages/" + dirName + "/ThumbPath/" + Path.GetFileName(file).ToLower());
                }
            }

            return "Done";
        }

        private PagedList<Photo> GetImages(string directoryName)
        {
            string filter = null;
            int page = 1; int pageSize = 20;
            var records = new PagedList<Photo>();
            ViewBag.filter = filter;

            var photos = new List<Photo>();
            try
            {
                var path = "../images/GalleryImages/" + directoryName + "/ImagePath";
                if (directoryName != null && Directory.Exists(Server.MapPath(path)))
                {
                    var files = Directory.GetFiles(Server.MapPath(path));
                    foreach (var file in files)
                    {
                        photos.Add(new Photo()
                        {
                            ImagePath = @"../images/GalleryImages/" + directoryName + "/ImagePath/" + Path.GetFileName(file).ToLower(),
                            Description = "",
                            PhotoId = 1,
                            CreatedOn = DateTime.Now,
                            ThumbPath = @"../images/GalleryImages/" + directoryName + "/ThumbPath/" + Path.GetFileName(file).ToLower(),
                        });
                    }
                }
            }
            finally {
                records.Content = photos;
                // Count
                records.TotalRecords = records.Content.Count;

                records.CurrentPage = page;
                records.PageSize = pageSize;
            }
            return records;
        }

        private void resizeImage(Image imgToResize, Size size, string filePath)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bitmap = new Bitmap(destWidth, destHeight);
            using (Graphics g = Graphics.FromImage((Image)bitmap))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
                g.Dispose();
            }

            using (MemoryStream stream = new MemoryStream())
            {
                bitmap.Save(stream, ImageFormat.Jpeg);
                stream.Position = 0;
                byte[] image = new byte[stream.Length + 1];
                stream.Read(image, 0, image.Length);
                using (FileStream fs = new FileStream(Server.MapPath(filePath), FileMode.Create, FileAccess.ReadWrite))
                {
                    fs.Write(image, 0, image.Length);
                    fs.Flush();
                    fs.Close();
                    fs.Dispose();
                }
            }
        }
    }
}
