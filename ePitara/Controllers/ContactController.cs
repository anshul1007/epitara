﻿using ePitara.Models;
using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web.Mvc;


namespace ePitara.Controllers
{
    public class ContactController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(ContactViewModel e)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var toAddress = new string[] { "siddhant.jain.ind@gmail.com", "anshul.1007@gmail.com" };
                    var subject = "Test enquiry from " + e.Name + " Subject:" + e.Subject;
                    var message = new StringBuilder();
                    message.Append("<b>Name:</b> " + e.Name + "<br/>");
                    message.Append("<b>Email:</b> " + e.Email + "<br/>");
                    message.Append("<b>Mobile:</b> " + e.Mobile + "<br/><br/>");
                    message.Append(e.Message);

                    //start email Thread
                    var tEmail = new Thread(() => SendEmail(toAddress, subject, message.ToString()));
                    tEmail.Start();

                    ModelState.Clear();
                    ViewBag.Message = "Thank you for contacting us ";
                }
                catch (Exception ex)
                {
                    ModelState.Clear();
                    ViewBag.Message = $" Sorry we are facing Problem here {ex.Message}";
                }
            }

            return View();
        }

        public void SendEmail(string[] toAddress,
                      string subject, string message)
        {
            try
            {
                GoDaddy(toAddress, "contact@epitara.in", subject, message);
                //Gmail(toAddress, "epitara.retailco@gmail.com", subject, message);
            }
            catch (Exception ex)
            {
                ViewBag.Message = $" Failed to deliver message to  {ex.Message}";
            }
        }

        private void Gmail(string[] toAddress, string fromAddress,
                      string subject, string message)
        {
            using (var mail = new MailMessage())
            {
                var smtpDetails = GetSmtpDetails(false);

                var loginInfo = new NetworkCredential(smtpDetails.Item3, smtpDetails.Item4);

                mail.From = new MailAddress(fromAddress);
                foreach (var s in toAddress)
                {
                    mail.To.Add(new MailAddress(s));
                }
                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = true;

                try
                {
                    using (var smtpClient = new SmtpClient(smtpDetails.Item1, smtpDetails.Item2))
                    {
                        smtpClient.EnableSsl = true;
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Credentials = loginInfo;
                        smtpClient.Timeout = 10000;//was 1000 initialy
                        smtpClient.Send(mail);
                    }
                }
                finally
                {
                    //dispose the client
                    mail.Dispose();
                }
            }
        }

        private void GoDaddy(string[] toAddress, string fromAddress,
                      string subject, string message)
        {
            using (var mail = new MailMessage())
            {
                mail.From = new MailAddress(fromAddress);
                foreach (var s in toAddress)
                {
                    mail.To.Add(new MailAddress(s));
                }
                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = true;
                var smtpDetails = GetSmtpDetails(true);
                // MailMessage instance to a specified SMTP server
                using (var smtp = new SmtpClient(smtpDetails.Item1, smtpDetails.Item2))
                {
                    smtp.Credentials = new NetworkCredential(smtpDetails.Item3, smtpDetails.Item4);
                    smtp.EnableSsl = false;
                    // Sending the email
                    smtp.Send(mail);
                }
            }
        }

        private Tuple<string, int, string, string> GetSmtpDetails(bool isServer)
        {
            if (isServer)
            {
                return new Tuple<string, int, string, string>("relay-hosting.secureserver.net", 25, "contact@epitara.in", "Siddhant@2017");
            }
            else
            {
                return new Tuple<string, int, string, string>("smtp.gmail.com", 587, "epitara.retailco@gmail.com", "Sidd@1234");
            }
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}